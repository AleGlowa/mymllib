import numpy as np
import matplotlib.pyplot as plt

def hypothesis(X, thetas):
    hypothesis = np.dot(X, thetas)
    return hypothesis

def visualize(X, y, hypo):
    plt.scatter(X[:, 1:], y)
    plt.plot(X[:, 1:], hypo, c='red')
    plt.title('linear regression')
    plt.xlabel('x-axis')
    plt.ylabel('y-axis')
    plt.xlim((0, 11))
    plt.ylim((-1, 10))
    plt.show()

def mse_cost(error):
    mse = (error**2).mean()
    return mse

def train(thetas, X, y, training_rate=0.001, iterations=500):
    X = np.insert(X, 0, 1.0, axis=1)
    for i in range(1, iterations + 1):
        hypo = hypothesis(X, thetas)
        error = hypo - y
        cost = mse_cost(error)
        gradients = (X.shape[0] / 2) * np.dot(X.T, error)
        thetas = thetas - training_rate * gradients
        if i % 100 == 0:
                print(cost, 'at ', i, ' iteration')
                visualize(X, y, hypo)

X = np.arange(1, 11, 1, dtype=np.float)[:, np.newaxis]
#y = np.full((10, 1), 5, dtype=np.float)
y = np.array([[1], [5], [8], [10], [7], [6], [5], [9], [12], [6]])
thetas = np.zeros((2, 1))
train(thetas, X, y)